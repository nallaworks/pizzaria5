"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/common/http");
var app_component_1 = require("./app.component");
var form_component_1 = require("./form/form.component");
var app_routes_1 = require("./app.routes");
var api_service_1 = require("./api.service");
var lista_component_1 = require("./lista/lista.component");
var bebidas_form_component_1 = require("./bebidas-form/bebidas-form.component");
var bebidas_list_component_1 = require("./bebidas-list/bebidas-list.component");
var pedidos_component_1 = require("./pedidos/pedidos.component");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                form_component_1.FormComponent,
                lista_component_1.ListaComponent,
                bebidas_form_component_1.BebidasFormComponent,
                bebidas_list_component_1.BebidasListComponent,
                pedidos_component_1.PedidosComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routes_1.AppRoutes,
                forms_1.FormsModule,
                http_1.HttpClientModule
            ],
            providers: [
                api_service_1.ApiService
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map