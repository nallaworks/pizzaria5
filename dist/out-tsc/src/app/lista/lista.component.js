"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("../api.service");
var router_1 = require("@angular/router");
var ListaComponent = /** @class */ (function () {
    function ListaComponent(apiService, router) {
        this.apiService = apiService;
        this.router = router;
        this.pizzas = [];
    }
    ListaComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.apiService.getPizza().subscribe(function (data) {
            _this.pizzas = data;
        }, function (error) {
            console.log(error);
        });
    };
    ListaComponent.prototype.remover = function (pizzaId) {
        this.apiService.removerPizza(pizzaId).subscribe(function (data) {
            console.log("removeu");
            location.reload();
        }, function (error) {
            console.log(error);
        });
    };
    ListaComponent = __decorate([
        core_1.Component({
            selector: 'app-lista',
            templateUrl: './lista.component.html',
            styleUrls: ['./lista.component.css']
        }),
        __metadata("design:paramtypes", [api_service_1.ApiService,
            router_1.Router])
    ], ListaComponent);
    return ListaComponent;
}());
exports.ListaComponent = ListaComponent;
//# sourceMappingURL=lista.component.js.map