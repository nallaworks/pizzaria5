"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var form_component_1 = require("../app//form/form.component");
var lista_component_1 = require("./lista/lista.component");
var bebidas_list_component_1 = require("./bebidas-list/bebidas-list.component");
var bebidas_form_component_1 = require("./bebidas-form/bebidas-form.component");
var pedidos_component_1 = require("./pedidos/pedidos.component");
exports.appRoutes = [
    { path: 'pizza', component: form_component_1.FormComponent },
    { path: 'lista-pizza', component: lista_component_1.ListaComponent },
    { path: 'lista-bebida', component: bebidas_list_component_1.BebidasListComponent },
    { path: 'form-bebida', component: bebidas_form_component_1.BebidasFormComponent },
    { path: 'pedidos', component: pedidos_component_1.PedidosComponent }
];
exports.AppRoutes = router_1.RouterModule.forRoot(exports.appRoutes, { useHash: true });
//# sourceMappingURL=app.routes.js.map