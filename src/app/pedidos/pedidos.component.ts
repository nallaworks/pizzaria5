import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {
  public pedido: any = {};
  public pizzas = [];
  public bebidas: Array<any> = [];
  public valorTotal = 0;

  public listaPedidos: Array<any> = [];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getPizza().subscribe((data:any) => {
      this.pizzas = data;
    
    });
    this.apiService.getBebida().subscribe((data:any)=> {
      this.bebidas = data;
    });

  }

  /**
   * calculaValor parseFloat()
   
   return (this.pedido.bebida.Preco + this.pedido.pizza.Preco);*/
  public calculaValor() {
    return (parseFloat(this.pedido.bebidas.Preco) + parseFloat(this.pedido.pizza.Preco));
    
  }
  /**
   * adiciona
   */
  public adiciona() {
    let total = this.calculaValor();
    this.listaPedidos.push({
  bebida: this.pedido.bebidas,
  pizza: this.pedido.pizza,
  total: this.calculaValor()

    });
    this.pedido = {};
    this.valorTotal += total;
    console.log(this.listaPedidos);
    
  }

/**
 * salva
 */
public salva() {
  
}

}
